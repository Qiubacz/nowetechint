<?php
session_start();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
    <?php 
            require "connection.php";
            connect();

           ?>
<html>
    <?php
	require "head.html";
	?>
    <body>
    <div id="kontener">
  <?php
    require "top.html";
    require "topnav.html";
    ?>
     
  <section id="contact" class="bg-dark py-5">         </section>
  <div class="container">
      <div class="row">
          <div class="col-lg-9">
            <h1 class="text-primary pb-3 text-center">Napisz do nas</h1>
              <p class="lead text-center"> Jeżeli masz jakieś pytania, propozycję proszę pisz śmiało</p>
         
              <form>
                  <div class="input-group input-group-lg mb-3">
                      <div class="input-group-prepend">
                          <span class="input-group-text">
                              <i class="fas fa-user"></i>
                          </span>
                      </div>
                      <input type="text" class="form-control" placeholder="Imie" rows="5">
                  </div>
                  <div class="input-group input-group-lg mb-3">
                      <div class="input-group-prepend">
                          <span class="input-group-text">
                              <i class="fas fa-user"></i>
                          </span>
                      </div>
                      <input type="text" class="form-control" placeholder="Nazwisko">
                  </div>
                  <div class="input-group input-group-lg mb-3">
                      <div class="input-group-prepend">
                          <span class="input-group-text">
                              <i class="fas fa-envelope"></i>
                          </span>
                      </div>
                      <input type="text" class="form-control" placeholder="Email">
                  </div>
                  <div class="input-group input-group-lg mb-3">
                      <div class="input-group-prepend">
                          <span class="input-group-text">
                              <i class="fas fa-pencil-alt"></i>
                          </span>
                      </div>
                      <textarea class="form-control" placeholder="Wiadomość" name="wiadomosc" rows="4"></textarea>
                  </div>
                  <input type="submit" value="Submit" class="btn btn-primart btn-block btn-lg">
              </form>
           
          </div>
          <div class="col-lg-3 align-self-center">
              <img src="logo.jpg" alt="" class="img-fluid">
          </div>


      </div>

  </div>
  <?php
     if(isset($_POST['wiadomosc']))
    {
        echo "Wiadomosc zostala wyslana";
    }
    else {
        echo "blad";
    }
  ?>
    <?php
        require "bottom.html";
    ?>
    </body>
</html>