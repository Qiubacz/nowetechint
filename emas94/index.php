<?php
session_start();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
      <?php 
            require "connection.php";
            connect();
            ?>

<html>
    <?php
	require "pages/head.html";
	?>

    <body>
        
        
        <div id="kontener">
  <?php
    require "pages/top.html";
    require "pages/topnav.html";
    ?>

    <!-- Autorzy -->
    <section id="authors" class="my-5 text-center">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="info-header mb-5">
                        <h1 class="text-primary pb-3">
                            Poznaj autorów strony:
                        </h1>
                        <p class="lead">
                            teskt o nas
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-md-6">
                    <div class="card">
                        <div class="card-body order-2">
                            <img src="pages/przemek.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                            <h3 class="text-muted">Przemysław Matkowski</h3>
                            <h5 class="text-muted">Programista</h5>
                            <p class="text-secondary">amerykański aktor, producent filmowy i scenarzysta, laureat Oscara dla najlepszego aktora pierwszoplanowego za rolę w filmie Zjawa w 2016 roku</p>
                            <div class="d-flex justify-content-center">
                                <div class="p-4">
                                    <a href="http://facebook.com">
                                    <i class="fab fa-facebook"></i>
                                    </a>
                                </div>
                                <div class="p-4">
                                    <a href="http://twitter.com">
                                    <i class="fab fa-twitter"></i>
                                    </a>
                                </div>
                                <div class="p-4">
                                    <a href="http://instagram.com">
                                    <i class="fab fa-instagram"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-md-6">
                    <div class="card">
                        <div class="card-body order-3">
                            <img src="pages/kuba.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
                            <h3 class="text-muted">Jakub Turoń</h3>
                            <h5 class="text-muted">Programista</h5>
                            <p class="text-secondary"> polski aktor teatralny, telewizyjny i filmowy, piosenkarz i wokalista zespołu Pączki w Tłuszczu. Założyciel i dyrektor artystyczny Teatru IMKA</p>
                            <div class="d-flex justify-content-center">
                                <div class="p-4">
                                    <a href="http://facebook.com">
                                    <i class="fab fa-facebook"></i>
                                    </a>
                                </div>
                                <div class="p-4">
                                    <a href="http://twitter.com">
                                    <i class="fab fa-twitter"></i>
                                    </a>
                                </div>
                                <div class="p-4">
                                    <a href="http://instagram.com">
                                    <i class="fab fa-instagram"></i>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
</div>


<!-- Latest compiled JavaScript -->

    <?php
    require "pages/bottom.html";
    ?>
    </body>
</html>
